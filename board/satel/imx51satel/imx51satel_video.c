/*
 * Copyright (C) 2012 Freescale Semiconductor, Inc.
 * Fabio Estevam <fabio.estevam@freescale.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <linux/list.h>
#include <asm/gpio.h>
#include <asm/arch/iomux-mx51.h>
#include <linux/fb.h>
#include <ipu_pixfmt.h>

#define MX51SATEL_LCD_BACKLIGHT	IMX_GPIO_NR(1, 3)

static struct fb_videomode const bonafide_wvga = {
	.name		= "CLAA07LC0ACW",
	.refresh	= 60,
	.xres		= 800,
	.yres		= 480,
	.pixclock	= 31250,
	.left_margin	= 86,
	.right_margin	= 42,
	.upper_margin	= 33,
	.lower_margin	= 10,
	.hsync_len	= 128,
	.vsync_len	= 2,
	.sync		= 0,
	.vmode		= FB_VMODE_NONINTERLACED
};

static struct fb_videomode const dvi = {
	.name		= "DVI panel",
	.refresh	= 60,
	.xres		= 1024,
	.yres		= 768,
	.pixclock	= 15385,
	.left_margin	= 220,
	.right_margin	= 40,
	.upper_margin	= 21,
	.lower_margin	= 7,
	.hsync_len	= 60,
	.vsync_len	= 10,
 	.sync		= 0,
 	.vmode		= FB_VMODE_NONINTERLACED
 };

void setup_iomux_lcd(void)
{
	/* DI2_PIN15 */
	imx_iomux_v3_setup_pad(MX51_PAD_DI_GP4__DI2_PIN15);

	/* Pad settings for DI2_DISP_CLK */
	imx_iomux_v3_setup_pad(NEW_PAD_CTRL(MX51_PAD_DI2_DISP_CLK__DI2_DISP_CLK,
			    PAD_CTL_PKE | PAD_CTL_DSE_MAX | PAD_CTL_SRE_SLOW));

	/* Turn on GPIO backlight */
	imx_iomux_v3_setup_pad(NEW_PAD_CTRL(MX51_PAD_GPIO1_3__GPIO1_3,
						NO_PAD_CTRL));
	gpio_direction_output(MX51SATEL_LCD_BACKLIGHT, 1);
}

int board_video_skip(void)
{
	int ret;
	char const *e = getenv("panel");

	if (e) {
		if (strcmp(e, "bonafide") == 0) {
			ret = ipuv3_fb_init(&bonafide_wvga, 1, IPU_PIX_FMT_RGB565);
			if (ret)
				printf("bonafide cannot be configured: %d\n", ret);
			return ret;
		}
	}

	/*
	 * 'panel' env variable not found or has different value than 'claa'
	 *  Defaulting to dvi output.
	 */
	ret = ipuv3_fb_init(&dvi, 0, IPU_PIX_FMT_RGB24);
	if (ret)
		printf("dvi cannot be configured: %d\n", ret);
	return ret;
}
